Інструкція з встановлення та використання:

1. Оберіть папку та перейдіть в неї використовуючи командну строку.
2. Клонуйте проект в папку: git clone https://tymurmardas@bitbucket.org/tymurmardas/course.git.
3. Виконайте компіляцію файлу: javac.exe MainModel.java.
4. Запустіть файл java -cp . MainModel.
5. В консолі введіть слово для пошуку серед індексованих файлів з папки /data.