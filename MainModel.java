//package pc.labs.course;

import java.io.*;
import java.nio.file.Path;
import java.util.*;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.CompletableFuture;

public class MainModel {
    // data dir with text files"
    public static String dataDir = System.getProperty("user.dir") + "\\data";

    /**
     * ConcurrentHashMap implements Map data structure and also provide thread safety like Hashtable.
     * It works by dividing complete hashtable array into segments or portions and allowing parallel access to those segments.
     * The locking is at a much finer granularity at a hashmap bucket level.
     * The value of the ConcurrentHashMap implements Queue.
     **/
    public static ConcurrentHashMap<String, Queue> indexHashT = new ConcurrentHashMap<>();
    // will store a list of datafiles to index
    public static List<String> filesList = new ArrayList<String>();
    // number of threads. can be set to use the available number on the machine.
    private static int threadsNumber = Runtime.getRuntime().availableProcessors();
    // private static int threadsNumber = 4;
    public static List<CompletableFuture<?>> threadsArray = new ArrayList<>();

    public static void main(String args[]) throws IOException
    {
        long timeMainStart = System.currentTimeMillis();

        // look for all files in a directory and make a list of them
        try (Stream<Path> paths = Files.walk(Paths.get(dataDir))) {
            paths
                .filter(Files::isRegularFile)
                .forEach(f -> {
                    filesList.add(String.valueOf(f));
                });
        }

        long timeIndexingStart = System.currentTimeMillis();
        System.out.println("Files list created. Time taken (ms): " + (timeIndexingStart - timeMainStart));
        System.out.println("Start indexing using " + threadsNumber + " thread(s).");

        // divide the number of files by numbers of threads. Each thread gets an equal chunk.
        int filesPerThread = (filesList.size() / threadsNumber) + 1;

        // give each thread a portion of files to index
        // threads sync method is a CompletableFuture class
        for (int i = 0; i < threadsNumber; i++) {
            int bounds[] = new int[2];
            bounds[0] = filesPerThread * i;
            bounds[1] = filesPerThread * (i + 1) - 1;

            // each thread receives a bounds array[from, to] to apply to a shared files list
            CompletableFuture<Void> indexThread = CompletableFuture.supplyAsync(() -> {return bounds;}).thenApplyAsync(first ->
            {
                for (int ii = bounds[0]; ii < bounds[1]; ii++) {
                    try {
                        indexFile(filesList.get(ii));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            });
            // an array of threads to join them later
            threadsArray.add(indexThread);
        }

        // create a combined Future using allOf() to wait for CompletableFuture threads to complete
        CompletableFuture<Void> allFutures = CompletableFuture.allOf(threadsArray.toArray(new CompletableFuture[0]));
        allFutures.join();

        long timeThreadsJoin = System.currentTimeMillis();
        System.out.println("Indexing finished. Time taken (ms): " + (timeThreadsJoin - timeIndexingStart));

        // search word input
        String testWord = "mytestword";
        System.out.println("\nYou can use the index now. Enter your word to search: ");
        Scanner scanner = new Scanner(System.in);
        testWord = scanner.nextLine();
        System.out.println("Your word is: " + testWord + ". Searching...");

        long timeIndexSearchStart = System.currentTimeMillis();
        // index search results
        if (indexHashT.containsKey(testWord)) {
            Queue<String> indexList = indexHashT.get(testWord);
            System.out.println("The word is found in next files: " + indexList.toString());
        } else {
            System.out.println("The word is not found.");
        }

        long timeIndexSearchFinish = System.currentTimeMillis();
        System.out.println("Index search time (ms): " + (timeIndexSearchFinish - timeIndexSearchStart));

        System.out.println("\nTotal time time (ms): " + (timeIndexSearchFinish - timeMainStart));
    }

    // read a file and index words from it
    private static void indexFile(String filePath) throws IOException {

        // update the fileslist
        filesList.add(filePath);

        // open the file
        FileInputStream fstream = new FileInputStream(filePath);

        // get a filename from full path
        String[] filePathBits = filePath.split("\\\\");
        String fileName = filePathBits[filePathBits.length - 1];

        // read file line by line
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        String strLine;
        while ((strLine = br.readLine()) != null)   {
            String[] words = strLine.split("\\W+");

            for (String word : words) {
                // if there is a key in the hashtable update its value with a new filename
                if (indexHashT.containsKey(word)) {
                    Queue<String> indexList = indexHashT.get(word);
                    // prevent same filename to be indexed multiple times for a same key word
                    // can be altered to index it multiple times so it can be used to make e.g. index weight
                    if (!indexList.contains(fileName))
                        indexList.add(fileName);
                // create a key->value pair if the word is new
                // key is a word, value is a collection with one fileName inside
                // then creates a collection and assign it as a value.
                } else {
                    Queue<String> indexList = new LinkedList<>();
                    indexList.add(fileName);
                    indexHashT.put(word.toLowerCase(), indexList);
                }
            }
        }
    }
}
